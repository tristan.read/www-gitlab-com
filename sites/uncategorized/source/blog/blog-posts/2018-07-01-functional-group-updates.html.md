---
title: "GitLab's Functional Group Updates - July"
author: Trevor Knudsen
author_gitlab: tknudsen
categories: company
image_title: '/images/blogimages/functional-group-update-blog-cover.jpg'
description: "The Functional Groups at GitLab give an update on what they've been working on in July"
canonical_path: "/blog/2018/07/01/functional-group-updates/"
tags: inside GitLab, functional group updates
---

<!-- beginning of the intro - leave it as is -->

Every day from Monday to Friday, right before our GitLab Team call, a different Functional Group gives an update to our team.

The format of these calls is simple and short where they can either give a presentation or quickly walk the team through their agenda.

<!-- more -->

<!-- end of the intro -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

### Product Team

[Presentation slides](https://about.gitlab.com/why/)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/LFtJYi2RYFk" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

### Support Team

[Presentation slides](https://docs.google.com/presentation/d/1hQcoknn8LSsVM3AA8yR9QxVsCgDbKm8X5ZTjynsmghc/edit?usp=sharing)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/Sv5YjjOJNRs" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

----

### Frontend Team

[Presentation slides](https://docs.google.com/presentation/d/1JvWMtprxRI5sb0hI1VlAvbeoT9rBA2ALoOGxUbNPvUE/edit?usp=sharing)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/mLbHsBg6f_I" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

Questions? Leave a comment below or tweet [@GitLab](https://twitter.com/gitlab)! Would you like to join us? Check out our [job openings](/jobs/)!

----

### Geo Team

[Presentation slides](https://docs.google.com/presentation/d/1lt2rlEdPLV4FGT_evujtnlJcJjtguMg3h8be1nZTV4I/edit?usp=sharing)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/XA_V63GXgF4" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

----

### General Team

[Presentation slides](https://docs.google.com/presentation/d/1E1i5t4aMUkv4Dc7OV6LdeqHVQNVSqXCMy9KE3ZppZ50/edit?usp=sharing)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/bz9epIhOSzE" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

----

### Cloud Native Alliance Team

[Presentation slides](https://docs.google.com/presentation/d/1Boum-sJsJxf6GQ-doIesr9CMsYkp34iPNX9AbQ4IPD8/edit#slide=id.p)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/uHrVyAoM3D0" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

----
