import Vue from 'vue/dist/vue.min.js'
import { SlpNavigation } from 'be-navigation/dist/be-navigation.umd.min.js'

// eslint-disable-next-line import/no-webpack-loader-syntax
const css = require("!raw-loader!sass-loader!../stylesheets/be-navigation.css.scss").default;

// Enable Vue devtools for development environments
if (process.env.NODE_ENV === 'development') {
    Vue.config.devtools = true;
}

// Register the component with Vue
Vue.component('SlpNavigation', SlpNavigation);

// We have to use Web components to keep old Slippers styles out of the new Slippers nav,
// https://about.gitlab.com/blog/2021/05/03/using-web-components-to-encapsulate-css-and-resolve-design-system-conflicts/
(function () {
    if (window.customElements) {
        customElements.define('be-navigation',
            class extends HTMLElement {
                constructor() {
                    super();
                    const template = document.getElementById('be-nav').content;
                    const shadowRoot = this.attachShadow({ mode: 'open' });
                    shadowRoot.innerHTML = `<style>${css}</style>`;
                    shadowRoot.appendChild(template.cloneNode(true));
                    const navigationTarget = shadowRoot.querySelector('#be-nav-target');
                    new Vue({ el: navigationTarget });
                }
            });
    }

    document.addEventListener('searchClick', () => {
        const search = document.querySelector('.js-search')
        search.parentElement.classList.toggle('search-visible')

        if (search.parentElement.classList.contains('search-visible')) {
            setTimeout(() => {
                search.focus()
            }, 350)
        }
    })
})()